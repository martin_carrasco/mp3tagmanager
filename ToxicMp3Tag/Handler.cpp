#pragma once
#include <boost/filesystem.hpp>
#include "Handler.h"
#include <fstream>
#include <iostream>

using namespace std;

Handler::~Handler(void){}
void Handler::ReadAllBytes(const char* filename)
{
	ifstream ifs;

	ifs.open(filename, ios::binary);

	tag.tag[3] = '\0';
	tag.title[30] = '\0';
	tag.artist[30] = '\0';
	tag.album[30] = '\0';
	tag.year[4] = '\0';
	tag.comment[30] = '\0';
	tag.genre[1] = '\0';

	ifs.seekg(-128, ios::end);
	ifs.read(tag.tag, 3);
	ifs.read(tag.title, 30);
	ifs.read(tag.artist, 30);
	ifs.read(tag.album, 30);
	ifs.read(tag.year, 4);
	ifs.read(tag.comment, 30);
	ifs.read(tag.genre, 1);
}
Handler::Handler(void){}
Handler::ID3tag Handler::getTag()
{
	return tag;
}
void Handler::setTag(ID3tag tg)
{
	tag = tg;
}