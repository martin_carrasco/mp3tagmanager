#include <vector>
#include "Handler.h"
#include <boost\filesystem.hpp>
#include <map>

using namespace boost::filesystem;

//static std::vector<Handler*> list = std::vector<Handler*>();
//static std::map<path, Handler*> map = std::map<path, Handler*>();

class pData
{
public:
	static std::vector<Handler*> list; //= std::vector<Handler*>();
	static std::map<path, Handler*> map; //= std::map<path, Handler*>();
	static class _init
	{
	public:
		_init()
		{
			map = std::map<path, Handler*>();
			list = std::vector<Handler*>();
		}
	} _initializer;
public:
	pData(void);
	static path findPathByHandler(Handler* h);
	static Handler* findHandlerByName(char* name);
};