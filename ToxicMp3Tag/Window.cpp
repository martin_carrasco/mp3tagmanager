#include "Window.h"
#include "pData.h"
//#include <fstream>

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Windows::Forms;
Window::Window(Form^ main)
{
	this->main = main;
	init();
}
void Window::setTitle(String^ title){ ttitle->Text = title; }
void Window::setAlbum(String^ album){ talbum->Text = album; }
void Window::setArtist(String^ artist){ tartist->Text = artist; }
void Window::setFilename(String^ filename){ tfilename->Text = filename; }
void Window::setComment(String^ comment){ tcomment->Text = comment; }
void Window::setYear(String^ year){ tyear->Text = year; }
void Window::setGenre(String^ genre){ tgenre->Text = genre; }
void Window::init()
{
	accept = gcnew Button();
	cancel = gcnew Button();

	accept->Size = Drawing::Size(50, 30);
	accept->Location = Drawing::Point(70, 350);
	accept->Text = "Accept";
	this->accept->Click += gcnew EventHandler(this, &Window::OnAcceptClick);
	
	cancel->Size = Drawing::Size(50, 30);
	cancel->Location = Drawing::Point(180, 350);
	cancel->Text = "Cancel";

	album = gcnew Label();
	artist = gcnew Label();
	title = gcnew Label();
	filename = gcnew Label();
	comment = gcnew Label();
	year = gcnew Label();
	genre = gcnew Label();

	filename->Location = Drawing::Point(10, 10);
	title->Location = Drawing::Point(10, 40);
	artist->Location = Drawing::Point(10, 70);
	album->Location = Drawing::Point(10, 100);
	year->Location = Drawing::Point(10, 130);
	genre->Location = Drawing::Point(10, 160);
	comment->Location = Drawing::Point(10, 210);

	album->Text = "Album:";
	artist->Text = "Artist:";
	title->Text = "Title:";
	filename->Text = "Filename:";
	year->Text = "Year: ";
	genre->Text = "Genre(number): ";
	comment->Text = "Comment: ";

	album->Size = Drawing::Size(40, 20);
	artist->Size = Drawing::Size(40, 20);
	title->Size = Drawing::Size(40, 20);
	filename->Size = Drawing::Size(60, 20);
	comment->Size = Drawing::Size(70, 20);
	year->Size = Drawing::Size(40, 20);
	genre->Size = Drawing::Size(70, 40);

	tartist = gcnew TextBox();
	talbum = gcnew TextBox();
	ttitle = gcnew TextBox();
	tfilename = gcnew TextBox();
	tcomment = gcnew TextBox();
	tyear = gcnew TextBox();
	tgenre = gcnew TextBox();

	talbum->Location = Drawing::Point(60, 100);
	tartist->Location = Drawing::Point(60, 70);
	ttitle->Location = Drawing::Point(60, 40);
	tfilename->Location = Drawing::Point(80, 10);
	tyear->Location = Drawing::Point(60, 130);
	tgenre->Location = Drawing::Point(90, 160);
	tcomment->Location = Drawing::Point(90, 210);

	this->ClientSize = System::Drawing::Size(300, 400);
	this->Name = L"Reader";
	this->Text = L"TagEditor";
	this->ResumeLayout(false);

	this->Controls->Add(album);
	this->Controls->Add(artist);
	this->Controls->Add(title);
	this->Controls->Add(filename);
	this->Controls->Add(comment);
	this->Controls->Add(year);
	this->Controls->Add(genre);
	this->Controls->Add(ttitle);
	this->Controls->Add(talbum);
	this->Controls->Add(tartist);
	this->Controls->Add(tfilename);
	this->Controls->Add(tyear);
	this->Controls->Add(tcomment);
	this->Controls->Add(tgenre);
	this->Controls->Add(accept);
	this->Controls->Add(cancel);
}
void Window::OnAcceptClick(Object^ sender, EventArgs^ e)
{
	Handler* h = new Handler();
	char* tagTitle = static_cast<char*>(Marshal::StringToHGlobalAnsi(ttitle->Text).ToPointer());
	char* tagAlbum = static_cast<char*>(Marshal::StringToHGlobalAnsi(talbum->Text).ToPointer());
	char* tagArtist = static_cast<char*>(Marshal::StringToHGlobalAnsi(tartist->Text).ToPointer());
	char* tagComment = static_cast<char*>(Marshal::StringToHGlobalAnsi(tcomment->Text).ToPointer());
	char* tagYear = static_cast<char*>(Marshal::StringToHGlobalAnsi(tyear->Text).ToPointer());
	char* tagGenre = static_cast<char*>(Marshal::StringToHGlobalAnsi(tgenre->Text).ToPointer());
	Handler::ID3tag tg = { 'TAG'
		, *tagTitle
		, *tagAlbum
		, *tagArtist
		, *tagYear
		, *tagComment
		, *tagGenre };
	h->setTag(tg);

	std::ofstream f(pData::findPathByHandler(h).string().c_str(), std::ios_base::out & std::ios_base::binary);
	f.seekp(-128, f.end);
	long size = f.tellp();
	f.write(tg.tag, 3);
	f.write(tg.title, 30);
	f.write(tg.album, 30);
	f.write(tg.artist, 30);
	f.write(tg.year, 4);
	f.write(tg.comment, 30);
	f.write(tg.genre, 1);

	MessageBox::Show(gcnew String("Saved to file!"), "Success", MessageBoxButtons::OK);
	this->Hide();
	main->Show();
	
	delete tagTitle;
	delete tagAlbum;
	delete tagArtist;
	delete tagComment;
	delete tagYear;
	delete tagGenre;
	delete h;
}
void Window::OnCancelClick(Object^ sender, EventArgs^ e)
{
	this->Hide();
	main->Show();
}