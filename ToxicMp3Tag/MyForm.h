#pragma once
#include "pData.h"
#include <iostream>
#include "Window.h"
#include <boost/filesystem.hpp>
#include <map>
#include <vector>

#define DEBUG 1

namespace ToxicMp3Tag {

	using namespace boost::filesystem;
	using namespace System::Runtime::InteropServices;
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;


	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	private:
		bool loadedTags = false;
		Handler* findHandlerByName(char* name)
		{
			for each(Handler* hand in pData::list)
			{
				if (!strcmp(hand->getTag().title, name))
				{
					return hand;
				}
			}
			return NULL;
		}
	public:
		MyForm(void)
		{
			pData* d = new pData();
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: FolderBrowserDialog^ b;
	private: System::Windows::Forms::Button^  button1;

	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::ListBox^  listBox1;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->listBox1 = (gcnew System::Windows::Forms::ListBox());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(12, 181);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Read Tags";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(174, 181);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(75, 23);
			this->button3->TabIndex = 2;
			this->button3->Text = L"Edit";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &MyForm::button3_Click);
			// 
			// listBox1
			// 
			this->listBox1->Location = System::Drawing::Point(25, 12);
			this->listBox1->Name = L"listBox1";
			this->listBox1->Size = System::Drawing::Size(234, 134);
			this->listBox1->TabIndex = 0;
			listBox1->SelectedItem = nullptr;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 262);
			this->Controls->Add(this->listBox1);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button1);
			this->Name = L"MyForm";
			this->Text = L"TagEditor";
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) 
	{
		b = gcnew FolderBrowserDialog();
		b->Description = "Browse music folder";
		if (b->ShowDialog() == Windows::Forms::DialogResult::OK)
		{
			char* name = static_cast<char*>(Marshal::StringToHGlobalAnsi(b->SelectedPath).ToPointer());
			path p(name);
			for (auto i = directory_iterator(p); i != directory_iterator(); i++)
			{
				if (!is_directory(i->path()))
				{
					Handler* h = new Handler();
					h->ReadAllBytes(i->path().string().c_str());
					Handler::ID3tag tag = h->getTag();
					pData::list.push_back(h);
					listBox1->Items->Add(gcnew String(tag.title));
				}
				else
					continue;
			}
		}
	}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) 
	{
		String^ str = listBox1->SelectedItem->ToString();
		int index = listBox1->FindString(str);
		if (index != -1)
		{
			listBox1->SetSelected(index, true);
			Handler* hand = this->findHandlerByName(static_cast<char*>(Marshal::StringToHGlobalAnsi(str).ToPointer()));
			Window^ w = gcnew Window(this);
			Handler::ID3tag t = hand->getTag();
			#if DEBUG
			std::cout << "Selected name: "<< static_cast<char*>(Marshal::StringToHGlobalAnsi(str).ToPointer()) << "_";
			std::cout << "Tag artist: " << t.artist << "_";
			std::cout << "Tag album: " << t.album << "_";
			std::cout << "Tag title: "<<t.title << "_";
			#endif
			w->setArtist(gcnew String(t.artist));
			w->setAlbum(gcnew String(t.album));
			w->setTitle(gcnew String(t.title));
			w->setComment(gcnew String(t.comment));
			w->setGenre(gcnew String(t.genre));
			w->setYear(gcnew String(t.year));
			w->setFilename(gcnew String(t.title));
			w->Show();
		}
		else
		{
			MessageBox::Show("No selected music file!", "Error", Windows::Forms::MessageBoxButtons::OK);
		}
	}
};
}
