#include <vector>
class Handler
{
public:
	typedef struct
	{
		char tag[4];
		char title[31];
		char album[31];
		char artist[31];
		char year[5];
		char comment[31];
		char genre[2];
	} ID3tag;
private:
	ID3tag tag;
public:
	~Handler(void);
	Handler(void);
	void setTag(ID3tag tg);
	ID3tag getTag(void);
	void ReadAllBytes(const char* filename);
	void clear(void);
};