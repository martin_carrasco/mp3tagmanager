using namespace System::Windows::Forms;
using namespace System;
ref class Window : System::Windows::Forms::Form
{
private:
	Button^ accept;
	Button^ cancel;
	TextBox^ tfilename;
	TextBox^ ttitle;
	TextBox^ talbum;
	TextBox^ tartist;
	TextBox^ tcomment;
	TextBox^ tyear;
	TextBox^ tgenre;
	Label^ filename;
	Label^ title;
	Label^ album;
	Label^ artist;
	Label^ comment;
	Label^ year;
	Label^ genre;
	Form^ main;
	void init();
	void OnAcceptClick(Object^ sender, EventArgs^ e);
	void OnCancelClick(Object^ sender, EventArgs^ e);
public:
	Window(Form^ main);
	void setTitle(String^ title);
	void setAlbum(String^ album);
	void setArtist(String^ artist);
	void setFilename(String^ filename);
	void setComment(String^ comment);
	void setYear(String^ year);
	void setGenre(String^ genre);
};