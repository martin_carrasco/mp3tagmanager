# **Mp3TagManager** #


### An MP3 tag handler's overview ###

* Desktop application to read the data from MP3 files, be able to modify it with set data and titles.
* Version 0.1

### Pending Features ###

* Sort files into a music especific directory.
* Read out information from the file/DB and edit file accordingly so it has accurate information (mainly for torrented files that come up with messed up info).
* Make it more eye appealing and easier to navigate.

### Contribution guidelines ###

* Feel free to contribute :)

### Who do I talk to? ###

* Owner of repo:
 martin_carrasco